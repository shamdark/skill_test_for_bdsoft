<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Districts extends CI_Controller{

    function __construct() {
        
        parent::__construct();
        if ($this->session->userdata('user_id') == '' && $this->session->userdata('user_name') == '') {
            redirect('Logins');
        }
        $this->load->library('pagination');
        $this->load->model(array('Division','District'), '', TRUE);
       
    }
   
    /**
    * Action for default District list view page
    * 
    * @uses    To view default District list page
    * @access   public
    * @param   void
    * @return  void
    * @author  Ahatasham
    */
   
    public function index(){

        $data = array();
        $data['user_role'] = $this->session->userdata();
        $data['heading'] = "Manage District";
        $data['title'] = "District List";
        $this->load->view('Districts/index',$data);
    }


    /**
    * Action for default District list through ajax call
    * 
    * @uses    To view default District through ajax call
    * @access   public
    * @param   void
    * @return  void
    * @author  Ahatasham
    */

     public function get_pagination_data($offset=0) {
        $per_page = 5;
        if($offset != 0){
            $offset = ($offset-1) * $per_page;
        }       
        $total_rows = $this->District->row_count();
        $data_list = $this->District->get_list($offset,$per_page);
        $config['base_url'] = base_url().'index.php/Districts/get_pagination_data';
        $config['use_page_numbers'] = TRUE;
        $config['next_link'] = '>>';
        $config['prev_link'] = '<<';
        $config['total_rows'] = $total_rows;
        $config['per_page'] = $per_page;
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        $data['record_list'] = $data_list;
        echo json_encode($data);        
    }
    
    /**
    * Action for add District data
    * 
    * @uses    To add District data
    * @access   public
    * @param   void
    * @return  void
    * @author  Ahatasham
    */

    public function add(){

        $this->_prepare_validation();

           if($_POST){  
                $data=$this->_get_posted_data();
                // echo "<pre>";print_r($data);die;
                if ($this->form_validation->run() === TRUE){
                    
                    if($this->District->add($data)){
                        $this->session->set_flashdata('message',ADD_MESSAGE);
                        
                    }
                    else{
                        $this->session->set_flashdata('warning',ADD_MESSAGE);   
                    }

                    redirect('/Districts/index');
                }
                else{
                   redirect('Districts');
                }  
            } 
        }

    /**
    * Action for get specific data by id for editing
    * 
    * @uses    To get specific data by id for editing
    * @access   public
    * @param   id: $id
    * @return  void
    * @author  Ahatasham
    */
   

    public function edit(){
        $id = $this->input->post('id');


           $callback_message = array();
           $district_details = $this->District->get_district_data($id);

           foreach ($district_details as $deatils) {
                $callback_message['district_name'] = $deatils->district_name;
                $callback_message['division_id'] = $deatils->division_id;
                $callback_message['id'] = $deatils->id;

              }
           $callback_message['get_all_division'] = $this->Division->get_division_data();

          echo json_encode($callback_message); 
         

    }

    /**
    * Action for update specific data by id 
    * 
    * @uses    To update specific data by id 
    * @access   public
    * @param   id: $id
    * @return  void
    * @author  Ahatasham
    */
       
    public function update(){

      if(isset($_POST)){
            $data = $this->_get_posted_data();
            $data['id'] = $this->input->post("id");

            if($this->District->edit_district($data)){
               $this->session->set_flashdata('message',EDIT_MESSAGE); 
             }

            else{
             $this->session->set_flashdata('message',WARNING_MESSAGE); 
            }
           
          redirect('Districts/index/', 'refresh');
      }
    }

    /**
    * Action for delete specific data by id 
    * 
    * @uses    To delete specific data by id 
    * @access   public
    * @param   id:$id
    * @return  void
    * @author  Ahatasham
    */
   
   
    public function delete_district($id=null){
        $callback_message = array();
        $id = $this->input->post('id');
        if(empty($id) || $id == ""){
            
         $this->session->set_flashdata('warning',WARNING_MESSAGE); 
         redirect('Districts/index', 'refresh');
        }

       
        if($this->District->delete_district($id))
            {
                // $this->session->set_flashdata('message',DELETE_MESSAGE);
                $callback_message['status']="success"; 
            }else{
               $this->session->set_flashdata('warning',WARNING_MESSAGE);
               $callback_message['status']="failed";
            }
         echo json_encode($callback_message);
    }
    

    /**
     * Action for maping the form data to database fields
     * 
     * @uses    To map the form data to database fields
     * @access  public
     * @param   void 
     * @return  array
     * @author  Ahatasham
     */ 
    
    function _get_posted_data(){
        $data=array();
        
        $data['division_id']=$this->input->post('division_id');
        $data['district_name']=$this->input->post('district_name');
              
        return $data;       
    }


    /**
     * Action for setting validation rules
     * 
     * @uses    To set validation rules
     * @access  private
     * @param   void 
     * @return  void
     * @author  Ahatasham
     */ 
    

    function _prepare_validation(){

        //Loading Validation Library to Perform Validation numeric
        
        $this->load->library('form_validation');    
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        //Setting Validation Rule
        $this->form_validation->set_rules('district_name','district_name','trim|xss_clean|required');  
        $this->form_validation->set_rules('division_id','division_id','trim|xss_clean|required');  
         
    }


    /**
     * Prepares data for combo
     * 
     * @uses    To load data
     * @access  public
     * @param   void 
     * @return  array
     * @author  Ahatasham
     */ 
    
    public function _load_combo_data(){

        $data['division_info'] = $this->District->division_info();     
        return $data;
    }

    /**
     * Get division list 
     * 
     * @uses    To save district data
     * @access  public
     * @param   void 
     * @return  array
     * @author  Ahatasham
     */

    public function division_info(){

        $callback_message['get_data'] = $this->Division->get_division_data();
        
        echo json_encode($callback_message);
    }


    /**
     * Edit data while inline editing through jquery
     * 
     * @uses    To edit district data
     * @access  public
     * @param   void 
     * @return  array
     * @author  Ahatasham
     */
    

    public function editdata(){
        $callback_message = array();
        If( $_SERVER['REQUEST_METHOD']  != 'POST'  ){
            redirect('table');
        }
        
        $id = $this->input->post('id',true);
        $title = $this->input->post('title',true); 
        $field_name = $this->input->post('field_name',true);       
        $fields = array($field_name => $title); 
        
        
        $this->District->edit_data($id,$fields);
        $callback_message['status'] = "success"; 
        $callback_message['division'] = $this->District->get_division_by_district_id($title);
          

        echo json_encode($callback_message); 
          
    }


    /**
     * Check user permission
     * 
     * @uses    To verify user has proper permission
     * @access  public
     * @param   void 
     * @return  array
     * @author  Ahatasham
     */

    public function role_wise_user_access_check(){

        $callback_message = array();
        $callback_message['permission'] = 0;

        $logged_user_info = $this->session->userdata();
        
        $division_id = $this->input->post('division_id');


        if($logged_user_info['is_admin'] == 1){
            $callback_message['permission'] = 1;
        }else{

            if($logged_user_info['is_admin'] != 1 && $logged_user_info['is_divisional_admin'] == 1){
                if($logged_user_info['division'] == $division_id){
                   $callback_message['permission'] = 1; 

                }
            }else{
                
                   $callback_message['permission'] = 0;  
                
            }
        }

    echo json_encode($callback_message);

    }
}