<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Logins extends CI_Controller{

    public function __construct(){

        parent::__construct();

        $this->load->model(array('Login'));

    }

    /*
    * @author: Ahatasham
    * return user login view page
    * 
    */

    public function index(){
    
       $this->load->view('Logins/index');
    }



    /**
    * validate user credential
    * @author: Ahatasham
    * @params : username, passwrod
    * 
    **/

    public function validate(){

        $admin_user = 0;
        $divisional_user = 0;

        $this->form_validation->set_rules('username', 'Username', 'required', 'trim');
        $this->form_validation->set_rules('password', 'Password', 'required');

        if($this->form_validation->run() == TRUE){

            $username=rtrim($_POST['username']);
            $password=rtrim($_POST['password']);

            $get_user = $this->db->query("SELECT * FROM `user` where name='$username' and password = '$password' and status=1  ")->result();

            if(!empty($get_user)){

                $logged_user_id = $get_user[0]->id;

                $user_info_by_id = $this->db->query("SELECT user.user_level, role.role_name FROM user JOIN role ON user.user_level=role.id WHERE user.id=$logged_user_id")->row();

                if($user_info_by_id->role_name == 'admin'){
                    $admin_user = 1;
                }

                elseif($user_info_by_id->role_name != 'admin' && $user_info_by_id->role_name == 'divisional_admin'){
                    $divisional_user = 1;
                }

                foreach($get_user as $row){
                    
                    $session_data = array(
                        'user_name' => $row->name,
                        'logged_name' => $row->name,
                        'user_level' => $row->user_level,
                        'user_role_name'=>$user_info_by_id->role_name,
                        'is_admin'=>$admin_user,
                        'is_divisional_admin' =>$divisional_user,
                        'division' => $row->division_id,
                        'district' => $row->district_id,
                        'user_id' =>$row->id 
                        
                    );
               
                $this->session->set_userdata($session_data);
                }

                redirect('Dashboards');
            } 

            else{

                $data = array();
                $data['credential_info'] = "Wrong Credential!";
                
                $this->load->view('Logins/index',$data);
            }          
            
        }

        else{

            $data = array();
            $data['credential_info'] = "Wrong Credential!";
            
            $this->load->view('Logins/index',$data);
        }
    }

    /**
    * logg out user by destroying session
    * @author: Ahatasham
    * @params : 
    * 
    **/

    public function logout(){

        $this->session->userdata('logged_in');

        $this->session->sess_destroy();
        redirect('Logins');
    }

    


}
