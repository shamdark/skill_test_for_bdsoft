<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class District extends CI_Model{

    function __construct() {
        // Call the Model constructor
        parent::__construct();
    }

    /**
     * Action for adding an district
     *
     * @uses    To add an district
     * @access  public
     * @param   array $data
     * @return  boolean
     * @author  Ahatasham
     */
    

    public function add($data) {
      return $this->db->insert('district', $data);
    }


    /**
    * Action for default District list 
    * 
    * @uses    To get default District list
    * @access   public
    * @param   void
    * @return  void
    * @author  Ahatasham
    */
   

    public function get_list($offset,$limit) {         
        $this->db->select("division.id as division_id, division.division_name,district.id as district_id, district.district_name");
        $this->db->from('district');
        $this->db->join('division', 'district.division_id = division.id');
        $this->db->limit($limit, $offset); 
        $this->db->order_by('district.id', 'DESC'); 
        $query = $this->db->get();          
        return $query->result_array();
    }

    /**
     * Count number of rows
     * @author  :   Ahatasham
     * @uses    :   To count row
     * @access  :   public
     * @return  :   int
     */

    
    function row_count() {
        return $this->db->count_all_results('district');
    }


    /**
     * get district list
     * @author  :   Ahatasham
     * @uses    :   district list anywhere needed
     * @access  :   public
     * @return  :   int
     */

    public function get_district_data($id=null){
        if(!empty($id)){
           $query = $this->db->query("SELECT * FROM district WHERE id = $id")->result(); 
        }
        else{
            $query = $this->db->query("SELECT * FROM district ORDER BY id DESC")->result();
        }
        
        return $query;
    }


    /**
     * get specific division 
     * @author  :   Ahatasham
     * @access  :   public
     * @return  :   int
     */

    public function get_division_by_district_id($id=null){
        if(!empty($id)){
           $query = $this->db->query("SELECT DISTINCT(division.division_name) AS division_name FROM district JOIN division ON district.division_id=division.id WHERE district.division_id = $id")->row()->division_name; 
           
        }
        return $query;
    }

    /**
     * Action for editing  district
     *
     * @uses    To edit  district
     * @access  public
     * @param   array $data
     * @return  boolean
     * @author  Ahatasham
     */
    
    public function edit_district($data){
        $this->db->where('id', $data['id']);
        return $this->db->update('district', $data);
    }

    /**
     * Action for deleting  district
     *
     * @uses    To delete district
     * @access  public
     * @param   id : $id
     * @return  boolean
     * @author  Ahatasham
     */

    public function delete_district($id){

        $this->db->where('id',$id);
        return $this->db->delete('district');

    }

    /**
     * Action for editing  district dynamically
     *
     * @uses    To delete district
     * @access  public
     * @param   id : $id, db_field_name : $fields
     * @return  boolean
     * @author  Ahatasham
     */

    public function edit_data($id,$fields){
          $this ->db->where('id',$id)->update('district',$fields);
    }

       
}