<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Division extends CI_Model{

    function __construct() {
        // Call the Model constructor
        parent::__construct();
    }

    /**
     * Action for adding an Division
     *
     * @uses    To add an Division
     * @access  public
     * @param   array $data
     * @return  boolean
     * @author  Ahatasham
     */
    
    public function add($data) {
        return $this->db->insert('division', $data);
    }


    /**
    * Action for default division list 
    * 
    * @uses    To get default division list
    * @access   public
    * @param   void
    * @return  void
    * @author  Ahatasham
    */


    public function get_list($offset,$limit) {         
        $this->db->select('*');
        $this->db->from('division');
        $this->db->limit($limit, $offset); 
        $this->db->order_by('division.id', 'DESC'); 
        $query = $this->db->get();          
        return $query->result_array();
    }


    /**
     * Count number of rows
     * @author  :   Ahatasham
     * @uses    :   To count row
     * @access  :   public
     * @return  :   int
     */

    
    function row_count($cond = '') {

        return $this->db->count_all_results('division');
    }

    /**
     * get division list
     * @author  :   Ahatasham
     * @uses    :   division list anywhere needed
     * @access  :   public
     * @return  :   int
     */

    public function get_division_data($id=null){

        if(empty($id)){
            $query = $this->db->query("SELECT * FROM division ORDER BY id DESC")->result();
        }
        else{
            $query = $this->db->query("SELECT * FROM division WHERE id = $id")->result();
        }
        
        return $query;
    }

    /**
     * Action for editing  division
     *
     * @uses    To edit  division
     * @access  public
     * @param   array $data
     * @return  boolean
     * @author  Ahatasham
     */
    

    public function edit_division($data){
        $this->db->where('id', $data['id']);
        return $this->db->update('division', $data);
    }

    /**
     * Action for deleting  division
     *
     * @uses    To delete division
     * @access  public
     * @param   id : $id
     * @return  boolean
     * @author  Ahatasham
     */

    public function delete_division($id){

        $this->db->where('id',$id);
        return $this->db->delete('division');

    }

    /**
     * Action for editing  division dynamically
     *
     * @uses    To delete division
     * @access  public
     * @param   id : $id, db_field_name : $fields
     * @return  boolean
     * @author  Ahatasham
     */

    public function edit_data($id,$fields){
          $this ->db->where('id',$id)->update('division',$fields);
    }



    
    
       
}