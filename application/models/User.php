<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class User extends CI_Model{

    function __construct() {
        // Call the Model constructor
        parent::__construct();
    }


    /**
     * Action for adding an User
     *
     * @uses    To add an User
     * @access  public
     * @param   array $data
     * @return  boolean
     * @author  Ahatasham
     */
    

    public function add($data) {
        return $this->db->insert('user', $data);
    }


    /**
    * Action for default User list 
    * 
    * @uses    To get default User list
    * @access   public
    * @param   void
    * @return  void
    * @author  Ahatasham
    */
   

    public function get_list($offset,$limit) {         
        $this->db->select("division.id as division_id, division.division_name,district.id as district_id, district.district_name,user.id as user_id,user.designation,role.role_name,role.id as role_id,user.name as user_name,user.id as user_id");
        $this->db->from('user');
        $this->db->join('division', 'user.division_id = division.id');
        $this->db->join('district', 'user.district_id = district.id');
        $this->db->join('role', 'user.user_level = role.id');
        $this->db->limit($limit, $offset); 
        $this->db->order_by('user.id', 'DESC'); 
        $query = $this->db->get();          
        return $query->result_array();
    }



    /**
     * Count number of rows
     * @author  :   Ahatasham
     * @uses    :   To count row
     * @access  :   public
     * @return  :   int
     */

   
    function row_count() {

        return $this->db->count_all_results('user');
    }


    /**
     * User role data
     * @author  :   Ahatasham
     * @uses    :   To count row
     * @access  :   public
     * @return  :   object
     */

    public function get_role_data($id=null){
        if(!empty($id)){
           $query = $this->db->query("SELECT * FROM role WHERE id = $id")->result(); 
        }
        else{
            $query = $this->db->query("SELECT * FROM role ORDER BY id DESC")->result();
        }
        
        return $query;
    }

    /**
     * Action for editing  district
     *
     * @uses    To edit  district
     * @access  public
     * @param   array $data
     * @return  boolean
     * @author  Ahatasham
     */


    public function edit_district($data){
        $this->db->where('id', $data['id']);
        return $this->db->update('district', $data);
    }


    /**
     * Action for deleting  user
     *
     * @uses    To delete user
     * @access  public
     * @param   id : $id
     * @return  boolean
     * @author  Ahatasham
     */

    public function delete_user($id){

        $this->db->where('id',$id);
        return $this->db->delete('user');

    }


    /**
     * Action for editing  user dynamically
     *
     * @uses    To delete user
     * @access  public
     * @param   id : $id, db_field_name : $fields
     * @return  boolean
     * @author  Ahatasham
     */
    
    public function edit_data($id,$fields){
          $this ->db->where('id',$id)->update('user',$fields);
    }



    /**
    * Action for  district list  with or without condition
    * 
    * @uses    To get  district list with or without condition
    * @access   public
    * @param   void
    * @return  void
    * @author  Ahatasham
    */


    public function get_district_list($division_id){
        $condition = "";
        if(!empty($division_id)) {
            $condition = " WHERE division_id = '$division_id' ";
        }
        $query = "SELECT district.id as district_id, district.district_name FROM district $condition";
        $results = $this->db->query($query);
        $results = $results->result_array();
        $districts = array();
        foreach($results as $row){
            $districts[$row['district_id']]['district_id'] = $row['district_id'];
            $districts[$row['district_id']]['district_name'] = $row['district_name'];
           
        }
        return $districts;
    }

    /**
    * Action for  role list  
    * 
    * @uses    To get  role list
    * @access   public
    * @param   void
    * @return  void
    * @author  Ahatasham
    */


    public function get_role_by_user_id($id=null){
        if(!empty($id)){
           $query = $this->db->query("SELECT DISTINCT(role.role_name) AS role_name FROM user JOIN role ON user.user_level=role.id WHERE user.user_level = $id")->row()->role_name; 
           
        }
        return $query;
    }

  /**
    * Action for  specific divison by user id
    * 
    * @uses    To get  division list
    * @access   public
    * @param   void
    * @return  void
    * @author  Ahatasham
    */
   

     public function get_division_by_user_id($id=null){
        if(!empty($id)){
           $query = $this->db->query("SELECT DISTINCT(division.division_name) AS division_name FROM user JOIN division ON user.division_id=division.id WHERE user.division_id = $id")->row()->division_name; 
           
        }
        return $query;
    } 

    /**
    * Action for  specific district by user id
    * 
    * @uses    To get  district list
    * @access   public
    * @param   void
    * @return  void
    * @author  Ahatasham
    */
   

    public function get_district_by_user_id($id=null){
        if(!empty($id)){
           $query = $this->db->query("SELECT DISTINCT(district.district_name) AS district_name FROM user JOIN district ON user.district_id=district.id WHERE user.district_id = $id")->row()->district_name; 
           
        }
        return $query;
    }

       
}