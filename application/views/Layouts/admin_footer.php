  </div>
  <!-- Main Footer -->
  <footer class="main-footer">
    <strong>Copyright &copy; 2020-2021 <a href="http://thejournalexpress24.com/portfolio">Md. Ahatasham</a>.</strong>
    All rights reserved.
    <div class="float-right d-none d-sm-inline-block">
      <b>Version</b> 3.0.5
    </div>
  </footer>
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->
<!-- jQuery -->
<script src="<?php echo base_url();?>plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="<?php echo base_url();?>plugins/bootstrap/js/bootstrap.bundle.min.js"></script>


<!-- DataTables -->
<script src="<?php echo base_url();?>plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url();?>plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="<?php echo base_url();?>plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url();?>plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<!-- overlayScrollbars -->
<script src="<?php echo base_url();?>plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url();?>dist/js/adminlte.js"></script>


<script src="<?php echo base_url();?>plugins/chart.js/Chart.min.js"></script>



<!-- PAGE SCRIPTS -->
<script src="<?php echo base_url();?>dist/js/pages/dashboard2.js"></script>
<script>
  $(function () {
    $("#example1").DataTable({
      "responsive": true,
      "autoWidth": false,
    });
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  });
</script>

<script>
    $("document").ready(function(){
    
    /* division part */

     $(document).on('click', '.add_division', function(){

            $('#add_division').modal('show');
        });


     $(document).on('click', '.edit_division', function(){
            var id = $(this).attr("id");

            $.post("<?php echo base_url();?>index.php/Divisions/edit", {id: id},
                function(data){
                    
                    $('#edit_division_name').val(data.division_name);
                    $('#id').val(data.id);


                }, "json");
            $('#edit_division').modal('show');
        });

    /*  District part */

     $(document).on('click', '.add_district', function(){
     		$.post("<?php echo base_url();?>index.php/Districts/division_info",
                function(data){
                    
                    if(data.get_data.length>0){

                        for(var i = 0; i < data.get_data.length; i++){

                            $('#add_division').append('<option value = \"' + data.get_data[i].id + '\">' + data.get_data[i].division_name + '</option>');
                        }
                    }else{
                        $('#add_division').append('<option value = "">--Select Division--</option>');
                    }


                }, "json");
            $('#add_district').modal('show');
        });


         $(document).on('click', '.edit_district', function(){
            var id = $(this).attr("id");

            $.post("<?php echo base_url();?>index.php/Districts/edit", {id: id},
                function(data){

                    var selected_item = data.division_id;
                    $("#edit_division").empty();
                    if(data.get_all_division.length>0){
                        for(var i = 0; i < data.get_all_division.length; i++){


                            if(data.get_all_division[i].id == selected_item){

                                selected = ' selected="selected" ';

                            }else{
                                selected = '';
                            }
                            $('#edit_division').append('<option value = \"' + data.get_all_division[i].id + '\" '+selected+'>' + data.get_all_division[i].division_name + '</option>');
                           
                        }
                    }else{
                        $('#edit_division').append('<option value = "">--Select Division--</option>');
                    }

                    $('#edit_district_name').val(data.district_name);

                    $('#id').val(data.id);


                }, "json");
            $('#edit_district').modal('show');
        });



      /* user part  */

       $(document).on('click', '.add_user', function(){
     		$.post("<?php echo base_url();?>index.php/Users/get_data_for_add_user",
                function(data){
                    
                    if(data.get_division_data.length>0){

                        for(var i = 0; i < data.get_division_data.length; i++){

                            $('#user_division').append('<option value = \"' + data.get_division_data[i].id + '\">' + data.get_division_data[i].division_name + '</option>');
                        }
                    }else{
                        $('#user_division').append('<option value = "">--Select Division--</option>');
                    }

                     if(data.get_role_data.length>0){

                        for(var i = 0; i < data.get_role_data.length; i++){

                            $('#add_admin_level').append('<option value = \"' + data.get_role_data[i].id + '\">' + data.get_role_data[i].role_name + '</option>');
                        }
                    }else{
                        $('#add_admin_level').append('<option value = "">--Select Role--</option>');
                    }


                }, "json");
            $('#add_user').modal('show');
        });

    });


</script>

<script>
function editdata_for_user(e,id,title,field_name) {
	
      $.ajax({
        type: "POST",
        url: "<?php echo base_url();?>index.php/Users/editdata",
        data: {  '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>',
                'id': id,
                'title' :title,
                'field_name' :field_name,
              },
        success: function(response){ 
          
        }
      });
 
}

function delete_user_data(e,id,division_id,district_id) {
	if (confirm('Are you sure to delete ?')) {
    $.post("<?php echo base_url();?>index.php/Users/role_wise_user_access_check", {id: id,division_id: division_id,district_id: district_id},
    function(data){
      if(data.permission == 1){
          $.post("<?php echo base_url();?>index.php/Users/delete_user", {id: id},
          function(data){
            if(data.status == "success"){
                      $("#user_delete_"+id).remove();   
                  }
                }, "json");
            }else{
              alert("Oops! You are not permitted to delete this user")
            }

          }, "json"); 
     }
      
  }




/*  for district module */

function editdata_for_district(e,id,title,field_name) {
       
      $.ajax({
        type: "POST",
        url: "<?php echo base_url();?>index.php/Districts/editdata",
        data: {  '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>',
                'id': id,
                'title' :title,
                'field_name' :field_name,
              },
        success: function(response){ 
         
        }
      });
}


function delete_district_data(e,id,division_id) {
	if (confirm('Are you sure to delete ?')) {

    $.post("<?php echo base_url();?>index.php/Districts/role_wise_user_access_check", {division_id: division_id},
    function(data){
       if(data.permission == 1){
             
    $.post("<?php echo base_url();?>index.php/Districts/delete_district", {id: id},
    function(data){
      if(data.status == "success"){
                $("#district_delete_"+id).remove();    
            }
          }, "json");     
             
            }else{
              alert("Oops! You are not permitted to Delete")
            }

        }, "json");
     }
      
  }


/*  for division module */

function editdata_for_division(e,id,title,field_name) {
       
      $.ajax({
        type: "POST",
        url: "<?php echo base_url();?>index.php/Divisions/editdata",
        data: {  '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>',
                'id': id,
                'title' :title,
                'field_name' :field_name,
              },
        success: function(response){ 
         
        }
      });
  }


function delete_division_data(e,id) {
	if (confirm('Are you sure to delete ?')) {

    $.post("<?php echo base_url();?>index.php/Divisions/role_wise_user_access_check", {id: id},
    function(data){
      if(data.permission == 1){
             
      $.post("<?php echo base_url();?>index.php/Divisions/delete_division", {id: id},
      function(data){
      if(data.status == "success"){
           $("#division_delete_"+id).remove();
                // window.location.reload();
                // createPagination(0);     
            }
          }, "json");     
             
            }else{
              alert("Oops! You are not permitted to Delete")
            }

          }, "json");

     }
      
  }


  function role_wise_user_access_check_for_division(district_id,division_id,specification,division){


    if(specification == 'division_id'){

    $.post("<?php echo base_url();?>index.php/Districts/role_wise_user_access_check", {division_id: division_id},
    function(data){
      if(data.permission == 1){
        $("#first_division_name_"+district_id).hide();
       $("#second_division_name_"+district_id).show();
      
       $.post("<?php echo base_url();?>index.php/Districts/division_info",
                function(data){
                    
                    if(data.get_data.length>0){

                        for(var i = 0; i < data.get_data.length; i++){

                            $("#sn_division_name_"+district_id).append('<option value = \"' + data.get_data[i].id + '\">' + data.get_data[i].division_name + '</option>');
                        }
                    }

                 
                  $("#sn_division_name_"+district_id).change(function(){  

                  var selected_division_id=$("#sn_division_name_"+district_id).val();
                  
                 

                  $.post("<?php echo base_url();?>index.php/Districts/editdata", {'id': district_id,'title' :selected_division_id,'field_name' :specification},
                    function(data){

                               // $("#division_name_"+district_id).hide();  
                               $("#first_division_name_"+district_id).show();
                                $("#first_division_name_"+district_id).text(data.division);
                                $("#second_division_name_"+district_id).hide();
                                 
                                
                       
                      }, "json");
                  });


              }, "json");
             
            }else{
              alert("Oops! You are not permitted to edit Division")
            }

          }, "json");  

     }

  if(specification == 'district_name'){
          $.post("<?php echo base_url();?>index.php/Districts/role_wise_user_access_check", {division_id: division_id},
    function(data){
       if(data.permission == 1){
             
               $("#ds_name_"+district_id).attr('contenteditable', 'true');     
             
            }else{
              alert("Oops! You are not permitted to edit Division")
            }

        }, "json");
     }


     if(specification == 'user_division_id'){

     }
   }


function role_wise_user_access_check(id,division_id,district_id,specification){

    var url = $(location).attr('href').split("/").splice(0, 6).join("/");
    var segments = url.split( '/' );
    var action = segments[5];

  if(action == 'Divisions'){
      $.post("<?php echo base_url();?>index.php/Divisions/role_wise_user_access_check", {id: id},
    function(data){
      if(data.permission == 1){
             
               $("#d_name_"+id).attr('contenteditable', 'true');     
             
            }else{
              alert("Oops! You are not permitted to edit Division")
            }

          }, "json");
    }
  if(action == 'Users'){
    $.post("<?php echo base_url();?>index.php/Users/role_wise_user_access_check", {id: id,division_id: division_id,district_id: district_id},
    function(data){
      if(data.permission == 1){
             if(specification == 'name'){
               $("#user_name_"+id).attr('contenteditable', 'true');     
             }
             if(specification == 'designation'){
               $("#designation_"+id).attr('contenteditable', 'true');  
             }
           
            }else{
              alert("Oops! You are not permitted to edit this user")
            }

          }, "json");  

      }    
  }



  function permission_wise_user_role_check(id,division_id,district_id,specification,role){

    
    $.post("<?php echo base_url();?>index.php/Users/role_wise_user_access_check", {id: id,division_id: division_id,district_id: district_id},
    function(data){
      if(data.permission == 1){

             if(specification == 'user_level'){
              $("#first_role_name_"+id).hide();
              $("#second_role_name_"+id).show();

                $.post("<?php echo base_url();?>index.php/Users/get_data_for_add_user",
                function(data){
                    
                    $("#sn_role_name_"+id).empty();
                     if(data.get_role_data.length>0){

                        for(var i = 0; i < data.get_role_data.length; i++){

                            $("#sn_role_name_"+id).append('<option value = \"' + data.get_role_data[i].id + '\">' + data.get_role_data[i].role_name + '</option>');
                        }
                    }

               $("#sn_role_name_"+id).change(function(){  

                  var selected_role_id=$("#sn_role_name_"+id).val();
                  
                 

                  $.post("<?php echo base_url();?>index.php/Users/editdata", {'id': id,'title' :selected_role_id,'field_name' :specification},
                    function(data){

                               // $("#division_name_"+district_id).hide();  
                               $("#first_role_name_"+id).show();
                                $("#first_role_name_"+id).text(data.role);
                                $("#second_role_name_"+id).hide();
                                 
                                
                       
                      }, "json");
                  });






                }, "json");



             }


      if(specification == 'division_id'){
      $("#first_user_division_name_"+id).hide();
       $("#second_user_division_name_"+id).show();
      
       $.post("<?php echo base_url();?>index.php/Districts/division_info",
                function(data){
                    
                    if(data.get_data.length>0){

                        for(var i = 0; i < data.get_data.length; i++){

                            $("#sn_user_division_name_"+id).append('<option value = \"' + data.get_data[i].id + '\">' + data.get_data[i].division_name + '</option>');
                        }
                    }

                 
                  $("#sn_user_division_name_"+id).change(function(){  

                  var selected_division_id=$("#sn_user_division_name_"+id).val();
                  
                 

                   $.post("<?php echo base_url();?>index.php/Users/editdata_for_division", {'id': id,'title' :selected_division_id,'field_name' :specification},
                    function(data){

                               // $("#division_name_"+district_id).hide();  
                               $("#first_user_division_name_"+id).show();
                                $("#first_user_division_name_"+id).text(data.divison);
                                $("#second_user_division_name_"+id).hide();
                                 
                                
                       
                      }, "json");
                  });


              }, "json");
             }


             if(specification == 'district_id'){
                      $("#first_user_district_name_"+id).hide();
       $("#second_user_district_name_"+id).show();
      
      $.post("<?php echo base_url();?>index.php/Users/get_data_for_add_user",
                function(data){
                    
                    if(data.get_district_data.length>0){

                        for(var i = 0; i < data.get_district_data.length; i++){

                            $("#sn_user_district_name_"+id).append('<option value = \"' + data.get_district_data[i].id + '\">' + data.get_district_data[i].district_name + '</option>');
                        }
                    }

                 
                  $("#sn_user_district_name_"+id).change(function(){  

                  var selected_district_id=$("#sn_user_district_name_"+id).val();
                  
                 

                   $.post("<?php echo base_url();?>index.php/Users/editdata_for_district", {'id': id,'title' :selected_district_id,'field_name' :specification},
                    function(data){

                               // $("#division_name_"+district_id).hide();  
                               $("#first_user_district_name_"+id).show();
                                $("#first_user_district_name_"+id).text(data.district);
                                $("#second_user_district_name_"+id).hide();
                                 
                                
                       
                      }, "json");
                  });


              }, "json");


             }

           
            }else{
              alert("Oops! You are not permitted to edit this user")
            }

          }, "json");      
  }



</script>

<script>
  $(document).click(function(event) { 
  var $target = $(event.target);
  if(!$target.closest('.second_role_td').length && 
  $('.second_role_td').is(":visible")) {
    $('.second_role_td').hide();
   $('.first_role_td').show();
  } 


  if(!$target.closest('.second_division_td').length && 
  $('.second_division_td').is(":visible")) {
    $('.second_division_td').hide();
   $('.first_division_td').show();
  }   

if(!$target.closest('.second_district_td').length && 
  $('.second_district_td').is(":visible")) {
    $('.second_district_td').hide();
   $('.first_district_td').show();
  }  


});
</script>

<script type='text/javascript'>

$(document).ready(function() {




	var url = $(location).attr('href').split("/").splice(0, 6).join("/");
    var segments = url.split( '/' );
    var action = segments[5];
	

  	createPagination(0);
  		$('#pagination').on('click','a',function(e){
  		e.preventDefault(); 
  		var offset = $(this).attr('data-ci-pagination-page');
  		createPagination(offset);
  	});

  	function createPagination(offset){
  		$.ajax({
  			url: '<?=base_url()?>index.php/'+action+'/get_pagination_data/'+offset,
  			type: 'get',
  			dataType: 'json',
  			success: function(response){
  				$('#pagination').html(response.pagination);
  				paginationData(response.record_list);
  			}
  		});
  	}

	 if(action=='Divisions'){

		function paginationData(data) {
			
			$('#division_table tbody').empty();

			for(list in data){			
				var empRow = "<tr id='division_delete_"+data[list].id+"'>";

				empRow += "<td>"+ data[list].id +"</td>";

			empRow += "<td id='d_name_"+data[list].id+"' title='Double click to Edit data' ondblclick=role_wise_user_access_check("+ data[list].id +",'','',''); onblur='this.contentEditable=false;' onkeyup=editdata_for_division(event,"+data[list].id+",$(this).html(),'division_name'); >"+ data[list].division_name +"</td>";

				empRow += "<td><a class='edit_division btn btn-info btn-xs' id='"+data[list].id+"'>"+ 'Edit' +"</a> <a class='btn btn-danger btn-xs' onClick=delete_division_data(event,"+data[list].id+",'division');>Delete</a></td>";
				
				empRow += "</tr>";

				$('#division_table tbody').append(empRow);					
			}
		}
	  }


	  if(action=='Districts'){

		function paginationData(data) {
			
			$('#district_table tbody').empty();

			for(list in data){			
				var empRow = "<tr id='district_delete_"+data[list].district_id+"'>";

				empRow += "<td>"+ data[list].district_id +"</td>";

				empRow += "<td class='first_role_td' id='first_division_name_"+data[list].district_id+"' title='Double click to Edit data' ondblclick=role_wise_user_access_check_for_division("+data[list].district_id+","+data[list].division_id+",'division_id',$(this).html()); this.className=inEdit; onblur='this.contentEditable=false;'); >"+ data[list].division_name +"<select style='display:none;' id='division_name_"+data[list].district_id+"'></select></td>";



         
        empRow += "<td class='second_role_td' style='display:none;' id='second_division_name_"+data[list].district_id+"' title='Double click to Edit data' ondblclick=role_wise_user_access_check_for_division("+data[list].district_id+","+data[list].division_id+",'division_id',$(this).html()); this.className=inEdit; onblur= display_previous_mode("+data[list].district_id+"); >"+ "<select id='sn_division_name_"+data[list].district_id+"'></select>" +"</td>";
 
         


				empRow += "<td id='ds_name_"+data[list].district_id+"' title='Double click to Edit data' ondblclick=role_wise_user_access_check_for_division("+data[list].district_id+","+data[list].division_id+",'district_name',$(this).html()); onblur='this.contentEditable=false;' onkeyup=editdata_for_district(event,"+data[list].district_id+",$(this).html(),'district_name'); >"+ data[list].district_name +"</td>";

				empRow += "<td><a class='edit_district btn btn-info btn-xs' id='"+data[list].district_id+"'>"+ 'Edit' +"</a> <a class='btn btn-danger btn-xs' id='"+data[list].district_id+"' onClick=delete_district_data(event,"+data[list].district_id+","+data[list].division_id+");>Delete</a></td>";
				
				empRow += "</tr>";

				$('#district_table tbody').append(empRow);					
			}
		}
	  }


	  if(action=='Users'){

		function paginationData(data) {
			
			$('#user_table tbody').empty();

			for(list in data){			
				var empRow = "<tr id='user_delete_"+data[list].user_id+"'>";

				empRow += "<td>"+ data[list].user_id +"</td>";

				empRow += "<td id='user_name_"+data[list].user_id+"' title='Double click to Edit data' ondblclick=role_wise_user_access_check("+data[list].user_id+","+data[list].division_id+","+data[list].district_id+",'name'); this.className=inEdit; onblur='this.contentEditable=false;' onkeyup=editdata_for_user(event,"+data[list].user_id+",$(this).html(),'name'); >"+ data[list].user_name +"</td>";

				empRow += "<td id='designation_"+data[list].user_id+"' title='Double click to Edit data' ondblclick=role_wise_user_access_check("+data[list].user_id+","+data[list].division_id+","+data[list].district_id+",'designation'); this.className=inEdit; onblur='this.contentEditable=false;' onkeyup=editdata_for_user(event,"+data[list].user_id+",$(this).html(),'designation'); >"+ data[list].designation +"</td>";



				empRow += "<td class='first_role_td' id='first_role_name_"+data[list].user_id+"' title='Double click to Edit data' ondblclick=permission_wise_user_role_check("+data[list].user_id+","+data[list].division_id+","+data[list].district_id+",'user_level',"+data[list].role_id+");>"+ data[list].role_name +"</td>";



        empRow += "<td class='second_role_td' style='display:none;' id='second_role_name_"+data[list].user_id+"'title='Double click and select any option to Edit data' ondblclick=permission_wise_user_role_check("+data[list].user_id+","+data[list].division_id+","+data[list].district_id+",'user_level',"+data[list].role_id+"); >"+ "<select  id='sn_role_name_"+data[list].user_id+"'></select>" +"</td>";





			 empRow += "<td class='first_division_td' id='first_user_division_name_"+data[list].user_id+"' title='Double click to Edit data' ondblclick=permission_wise_user_role_check("+data[list].user_id+","+data[list].division_id+","+data[list].district_id+",'division_id',"+data[list].role_id+"); >"+ data[list].division_name +"</td>";


       empRow += "<td class='second_division_td' style='display:none;' id='second_user_division_name_"+data[list].user_id+"' title='Double click to Edit data' ondblclick=permission_wise_user_role_check("+data[list].user_id+","+data[list].division_id+","+data[list].district_id+",'division_id',"+data[list].role_id+"); >"+ "<select  id='sn_user_division_name_"+data[list].user_id+"'></select>" +"</td>";


        



				empRow += "<td class='first_district_td' id='first_user_district_name_"+data[list].user_id+"' title='Double click to Edit data' ondblclick=permission_wise_user_role_check("+data[list].user_id+","+data[list].division_id+","+data[list].district_id+",'district_id',"+data[list].role_id+"); >"+ data[list].district_name +"</td>";



      empRow += "<td  class='second_district_td' style='display:none;' id='second_user_district_name_"+data[list].user_id+"'title='Double click to Edit data' ondblclick=permission_wise_user_role_check("+data[list].user_id+","+data[list].division_id+","+data[list].district_id+",'district_id',"+data[list].role_id+"); >"+ "<select  id='sn_user_district_name_"+data[list].user_id+"'></select>" +"</td>";




				empRow += "<td><a class='btn btn-danger btn-xs' id='"+data[list].user_id+"' onClick=delete_user_data(event,"+data[list].user_id+","+data[list].division_id+","+data[list].district_id+");>Delete</a></td>";
				
				empRow += "</tr>";

				$('#user_table tbody').append(empRow);					
			}
		}
	  }


});

</script>
<script>
  $(document).ready(function() {
   $("#user_division").change(function(){
     var selected_division_id=$("#user_division").val();

  $.post("<?php echo base_url();?>index.php/Divisions/role_wise_user_access_check", {id: selected_division_id},
    function(data){
      if(data.permission == 1){
             
        $('#status').html("");
        $('#user_district').empty();
        $.post("<?php echo base_url() ?>index.php/Users/get_district_id_by_division_id", {selected_division_id: selected_division_id},
            function(data){
                
                if(data.district_id.length>0)  {
                    for(var i = 0; i < data.district_id.length; i++){
                       
                        $('#user_district').append('<option value = \"' + data.district_id[i] + '\">' + data.district_name[i] + '</option>');
                       
                    }
                }
            }, "json");    
             
            }

            else{
              alert("Oops! You are not permitted to different division user")
            }

          }, "json");



   
      

        });


  $("#add_division").change(function(){
     var selected_division_id=$("#add_division").val();

  $.post("<?php echo base_url();?>index.php/Divisions/role_wise_user_access_check", {id: selected_division_id},
    function(data){
      if(data.permission != 1){  
         alert("Oops! You are not permitted to different division user");
           $("#add_district").attr('readonly','readonly');
            }

            

          }, "json");

        });
    });

</script>

</body>
</html>
