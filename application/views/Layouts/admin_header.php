<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">

  <title>BDSOFT SKILL TEST</title>

  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="<?php echo base_url();?>plugins/fontawesome-free/css/all.min.css">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="<?php echo base_url();?>plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url();?>dist/css/adminlte.min.css">
   <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo base_url();?>plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="<?php echo base_url();?>plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

  <style>
    .add_button{
      float:right;
    }
    .sham{
      display: none;
    }

    #pagination{
      margin-top:2%;
      /*float: right;*/
    }

    #pagination a{
      color: #fff;
      background-color: #17a2b8;
      border-color: #17a2b8;
      border-radius: 41px;
      padding: .2rem .5rem;
    }

    #pagination strong{
      color: #fff;
      background-color: red;
      border-color: #007bff;
      border-radius: 41px;
      padding: .2rem .5rem;
    }


  </style>
</head>
<body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed">
<div class="wrapper">
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>

      <li class="nav-item ">
        <h6 style="color:red;""> after changing user role please logout and login again. </h6>
      </li>
     
    </ul>

    <!-- Right navbar links -->
    
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="" style="text-align: center" class="brand-link">
      
      <span class="brand-text text-center font-weight-light">BDSOFT LTD</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="<?php echo base_url();?>dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block"><?php echo $this->session->userdata('user_name');?></a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
         
        <li class="nav-item">
            
                <a href="<?php echo base_url();?>index.php/Dashboards" class="nav-link active">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Dashboard </p>
                </a>
              
          </li>

          
          <li class="nav-item">
            <a href="<?php echo base_url();?>index.php/Divisions/index" id="division_menu" class="nav-link">
              <i class="nav-icon fas fa-th"></i>
              <p>
                Manage Division
               
              </p>
            </a>
          </li>

          <li class="nav-item">
            <a href="<?php echo base_url();?>index.php/Districts/index" class="nav-link">
              <i class="nav-icon fas fa-th"></i>
              <p>
                Manage District
               
              </p>
            </a>
          </li>


          <li class="nav-item">
            <a href="<?php echo base_url();?>index.php/Users/index" class="nav-link">
              <i class="nav-icon fas fa-th"></i>
              <p>
                Manage User
               
              </p>
            </a>
          </li>


          <li class="nav-item">
            <a href="<?php echo base_url();?>index.php/Logins/logout" class="nav-link">
              <i class="nav-icon fas fa-th"></i>
              <p>
                Logout
               
              </p>
            </a>
          </li>

      
          

        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#"><?php if(isset($heading) && !empty($heading)){ echo $heading;}?></a></li>
              <li class="breadcrumb-item active"><?php if(isset($title) && !empty($title)){ echo $title;}?></li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->

      <?php
        //Site Message
        $error = $this->session->flashdata('error');
        if (!empty($error)) echo "<div class='message_head'><div class='error'>$error</div></div>";
        $warning = $this->session->flashdata('warning');
        if (!empty($warning)) echo "<div class='message_head alert alert-warning'><div class='warning'>$warning</div></div>";
        $notice = $this->session->flashdata('notice');
        if (!empty($notice)) echo "<div class='message_head'><div class='notify'>$notice</div></div>";
        $information = $this->session->flashdata('information');
        if (!empty($information)) echo "<div class='message_head'><div class='info'>$information</div></div>";
        $success = $this->session->flashdata('success');
        if (!empty($success)) echo "<div class='message_head'><div class='message'>$success</div></div>";
        $message = $this->session->flashdata('message');
        if (!empty($message)) echo "<div class='message_head alert alert-success'><div class='message'>$message</div></div>";
     ?>
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

