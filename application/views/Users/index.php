<?php $this->load->view('Layouts/admin_header'); ?>
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">

        <div class="row justify-content-center">
          <div class="col-12">


                   <!--------------add branch modal------------------------------------------------------------------------>

                      <div id="add_user" class="modal fade">
                          <div class="modal-dialog">
                              <div class="modal-content" >
                                  <div class="modal-header">
                                      <h5 class="text-left"> Add User</h5>
                                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                                  </div>
                                  <div class="modal-body">
                                      <form name="frm" id="frm" action="<?php echo base_url();?>index.php/Users/add" method="post"
                                        enctype="multipart/form-data">


                                      <div class="form-row">
                                          <div class="form-group col-md-6">
                                              <label for="inputEmail4">User Name </label>
                                               <input type="text" name="name" id="add_name" class="form-control"/>
                                          </div>

                                          <div class="form-group col-md-6">
                                              <label for="inputEmail4"> Designation </label>
                                               <input type="text" name="designation" id="add_designation" class="form-control"/>
                                          </div>
                                      </div>


                                      <div class="form-row">
                                          <div class="form-group col-md-6">
                                            <label for="inputEmail4">Admin Role </label>
                                              <select required="required" name="admin_level" class="form-control"   id="add_admin_level"> 
                                                  <option value="">---- Select Admin Level ----</option>
                                                  
                                              </select>
                                          </div>

                                          <div class="form-group col-md-6">
                                            <label for="inputEmail4">Division Name </label>
                                              <select required="required" name="division_id" class="form-control"   id="user_division"> 
                                                  <option value="">---- Select Division ----</option>
                                              </select>
                                          </div>
                                        </div>



                                        <div class="form-row">
                                          <div class="form-group col-md-6">
                                            <label for="inputEmail4">District Name </label>
                                              <select required="required" name="district_id" class="form-control"   id="user_district"> 
                                                  <option value="">---- Select District ----</option>
                                              </select>
                                          </div>

                                          <div class="form-group col-md-6">
                                              <label for="inputEmail4">Password </label>
                                               <input type="password" name="password" id="add_password" class="form-control"/>
                                          </div>

                                          
                                        </div>



                                      <div class="form-row text-center">
                                          <div class="form-group col-md-12">
                                              <input type="submit" name="Submit" value="Save" class="btn btn-info"/>

                                          </div>
                                      </div>
                                  </form>
                              </div>

                          </div>
                      </div>
                  </div>

              <!----------------------end add branch modal ----------------------------------------------->




                  <!--------------edit branch modal------------------------------------------------------------------------>

                  <div id="edit_district" class="modal fade">
                      <div class="modal-dialog">
                          <div class="modal-content">
                              <div class="modal-header">
                                   <h5 class="text-left"> Update Division Name</h5>
                                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                              </div>
                              <div class="modal-body">
                                  <form name="frm" id="frm" action="<?php echo base_url();?>index.php/Districts/update" method="post"
                                    enctype="multipart/form-data">


                                    <div class="form-row">
                                          <div class="form-group col-md-12">
                                            <label for="inputEmail4">Division Name </label>
                                            <select required="required" name="division_id" class="form-control"   id="edit_division"> 
                                                  <option value="">---- Select Division ----</option>
                                              </select>
                                          </div>

                                          
                                    </div>

                                    <div class="form-row">
                                      <div class="form-group col-md-12">
                                          <label for="inputEmail4">District Name </label>
                                           <input type="text" name="district_name" id="edit_district_name" class="form-control"/>
                                      </div>

                                    
                                  </div>


                                  <input type="hidden" name="id" id="id"/>


                                  <div class="form-row text-center">
                                      <div class="form-group col-md-12">
                                          <input type="submit" name="Submit" value="Save" class="btn btn-info"/>

                                      </div>
                                  </div>
                              </form>
                          </div>

                      </div>
                  </div>
              </div>

               <!----------------------end edit modal ------------------------------------------->
           
            <!-- /.card -->

            <div class="card">
              <div class="card-header">
                <h3 class="card-title">DataTable with default features</h3>
                
              <?php if(isset($user_role) && ($user_role['is_admin']== 1 || $user_role['is_divisional_admin']== 1)){?> <Button class="btn btn-info btn-sm add_button add_user">Add </Button>
              <?php }?>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="user_table" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>Serial</th>
                    <th>User Name</th>
                    <th>Designation</th>
                    <th>User Level</th>                   
                    <th>Division Name</th>
                    <th>District Name</th>
                    <th>Action</th>
                  
                  </tr>
                  </thead>
                  <tbody>
                 
                 
                  </tbody>
                 
                </table>

               <div id='pagination' class="text-center"></div> 

              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->

     
  <?php $this->load->view('Layouts/admin_footer'); ?>