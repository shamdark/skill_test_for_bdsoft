/*
SQLyog Enterprise - MySQL GUI v8.12 
MySQL - 5.7.31-0ubuntu0.16.04.1 : Database - bdsoft
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

CREATE DATABASE /*!32312 IF NOT EXISTS*/`bdsoft` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `bdsoft`;

/*Table structure for table `district` */

DROP TABLE IF EXISTS `district`;

CREATE TABLE `district` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `division_id` int(11) DEFAULT NULL,
  `district_name` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

/*Data for the table `district` */

insert  into `district`(`id`,`division_id`,`district_name`) values (1,2,'Fulbaria'),(3,3,'Vate mora                                                   '),(4,19,'Narayanganj'),(5,19,'Madaripur'),(6,19,'Munshiganj'),(7,19,'Faridpur'),(8,19,'Gazipur'),(9,19,'Gopalganj'),(10,19,'Kishorganj'),(11,17,'Jamalpur'),(12,17,'Sherpur'),(13,17,'Netrokona'),(14,17,'Mymensingh Sadar'),(15,16,'Laxmipur'),(16,16,'Rangamati'),(17,16,'Bandarban'),(18,16,'Comilla'),(22,11,'Feni');

/*Table structure for table `division` */

DROP TABLE IF EXISTS `division`;

CREATE TABLE `division` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `division_name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

/*Data for the table `division` */

insert  into `division`(`id`,`division_name`) values (11,'Rajshahi'),(12,'Sylhet'),(13,'Rangpur'),(14,'Barishal'),(15,'Khulna'),(16,'Chittagong'),(17,'Mymensingh'),(19,'Dhaka');

/*Table structure for table `role` */

DROP TABLE IF EXISTS `role`;

CREATE TABLE `role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `role` */

insert  into `role`(`id`,`role_name`) values (1,'admin'),(2,'divisional_admin'),(3,'district_admin'),(4,'Select Role');

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) DEFAULT NULL,
  `user_level` varchar(30) DEFAULT NULL,
  `designation` varchar(50) DEFAULT NULL,
  `division_id` varchar(50) DEFAULT NULL,
  `district_id` varchar(60) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

/*Data for the table `user` */

insert  into `user`(`id`,`name`,`user_level`,`designation`,`division_id`,`district_id`,`password`,`status`) values (4,'Ahatasham','2','Divisional User','19','10','1',1),(6,'Rahim','3','District User','19','10','1',1),(7,'Karim','2','Divisional User','16','22','1',1),(8,'Sagor','2','Divisional Admin<br>','16','18','1',1),(9,'Shetu','1','Divisional User','15','17','1',1),(10,'Sham','1','Divisional Admin','17','14','1',1),(11,'Sabbir','3','District User','19','18','1',1),(12,'sajiud','2','Field oofficer','19','22','1',1);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
